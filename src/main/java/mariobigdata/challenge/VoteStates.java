package mariobigdata.challenge;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import scala.Tuple2;

public class VoteStates {
	static Logger log = Logger.getLogger(VoteStates.class.getName());

	public static void main(String[] args) {
		// Step 1: Create a SparkConf object
		if (args.length < 1) {
			log.fatal("Syntax Error: there must be one argument)");
			throw new RuntimeException();
		}

		// Step 2: create a SparkConf object

		SparkConf sparkConf = new SparkConf().setAppName("Most Voted");

		// Step 3: create a Java Spark context

		JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);

		JavaRDD<String> lines = sparkContext.textFile(args[0]);
		
		//Split all the lines of the document by :
		
		String header = lines.first();
		
		JavaRDD<String[]> split = lines.filter(row -> !row.equalsIgnoreCase(header)).map(row -> row.split((",")));
				
	    //Create a tuple with state name: 
		JavaRDD<String> state = split.map(s->s[0]);
		List<String> allstates = state.collect();
		JavaPairRDD<String, Integer> mapstates = split.mapToPair(s -> new Tuple2<String, Integer>(s[0],1));
		List<Tuple2<String, Integer>> reducestates = mapstates.reduceByKey((integer1,integer2) -> integer1 + integer2).sortByKey(true).collect();
		
		
		//We get the states before, so now we will filter by the state. Then create a tuple with the name of candidate and the votes. Sort by each candidate
		//and get the first. 
		int pos=0;
		for (Tuple2<?,?> tuple : reducestates) {
		    pos=0;    
			for(int i = 0; i<allstates.size(); i++){
				   
					if(tuple._1().equals(allstates.get(i))){
						
						if(pos==0){
						JavaRDD<String[]> mapstate = lines.filter(row -> row.contains((CharSequence) tuple._1())).map(row -> row.split((",")));
									
						
						//Create a tuple with candidate name and votes in each city.
						JavaPairRDD<String, Integer> mapcandidates = mapstate.mapToPair(s -> new Tuple2<String, Integer>(s[5],Integer.parseInt(s[6])));
						
						//Count the votes of each candidates.
						JavaPairRDD<String, Integer> sortcandidates = mapcandidates.reduceByKey((integer1,integer2) -> integer1 + integer2);
						Tuple2<Integer, String> votes1 = sortcandidates.mapToPair(s -> new Tuple2<Integer, String>(s._2(),s._1())).sortByKey(false).first();
						
							  System.out.println("State: " + tuple._1());
						      System.out.println("	Candidate:" + votes1._2() + " Votes: " + votes1._1());
					    pos = 1;
						}
						
					}
			}	
		
		}
		
		
		sparkContext.stop();

	}

}
