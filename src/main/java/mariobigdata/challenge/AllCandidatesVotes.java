package mariobigdata.challenge;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import scala.Tuple2;



public class AllCandidatesVotes {

	static Logger log = Logger.getLogger(AllCandidatesVotes.class.getName());
	
	public static void main(String[] args) {
		
		// Step 1: Create a SparkConf object
				if (args.length < 1) {
					log.fatal("Syntax Error: there must be one argument)");
					throw new RuntimeException();
				}

				// Step 2: create a SparkConf object

				SparkConf sparkConf = new SparkConf().setAppName("Most Voted");

				// Step 3: create a Java Spark context

				JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);

				JavaRDD<String> lines = sparkContext.textFile(args[0]);
				
				//Split all the lines of the document by :
				
				String header = lines.first();
				
				JavaRDD<String[]> split = lines.filter(row -> !row.equalsIgnoreCase(header)).map(row -> row.split((",")));
						
			    //Create a tuple with candidate name and votes in each city.
				JavaPairRDD<String, Integer> mapcandidates = split.mapToPair(s -> new Tuple2<String, Integer>(s[5],Integer.parseInt(s[6])));
				
				//Count the votes of each candidates.
				JavaPairRDD<String, Integer> sortcandidates = mapcandidates.reduceByKey((integer1,integer2) -> integer1 + integer2);
				List<Tuple2<Integer, String>> votes = sortcandidates.mapToPair(s -> new Tuple2<Integer, String>(s._2(),s._1)).sortByKey(false).collect();
				
				
				//Show the results
				for (Tuple2<?,?> tuple : votes) {
				      System.out.println(tuple._2() + ": " + tuple._1());
				    }
				
				
				sparkContext.stop();
				


	}

}
