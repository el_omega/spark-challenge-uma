# PRIMARY RESULTS OF US ELECTIONS #


### Description of the dataset ###

Each row contains the votes and fraction of votes that a candidate received in a given county's primary. sample

* state: state where the primary or caucus was held
* state_abbreviation: two letter state abbreviation
* county: county where the results come from
* fips: FIPS county code
* party: Democrat or Republican
* candidate: name of the candidate
* votes: number of votes the candidate received in the corresponding state and county (may be missing)
* fraction_votes: fraction of votes the president received in the corresponding state, county, and primary

### Analyses: ###

* AllCandidatesVotes: In this script we get the amount of votes of each candidate.
* VoteStates: Get the winner in each state.
* RepresentCandidate: Get the represent to candidate for each party: Republican and Democratic.

### Results ###

* AllCandidatesVotes:

Here, we can see that the most voted in the US is Hillary Clinton. But Donald Trump and Bernie Sanders are very close.

1. Hillary Clinton: 15692452
2. Donald Trump: 13302541
3. Bernie Sanders: 11959102
4. Ted Cruz: 7603006
5. John Kasich: 4159949
6. Marco Rubio: 3321076
7. Ben Carson: 564553
8. Jeb Bush: 94411
9. Chris Christie: 24353
10. Carly Fiorina: 15191
11. Rand Paul: 8479
12.  No Preference: 8152
13. Mike Huckabee: 3345
14. Rick Santorum: 1782
15. Martin O'Malley: 752
16.  Uncommitted: 43


* VoteStates:

Now, we can see the winner in each state. In the US elections, the candidate who wins in more states become in president.
There are a lot of states, so I show here some of them.

State: Alabama
	 Candidate:Donald Trump Votes: 371735
State: Alaska
 	Candidate:Ted Cruz Votes: 7973
State: Arizona
 	Candidate:Donald Trump Votes: 249916
State: Arkansas
	 Candidate:Hillary Clinton Votes: 144580
State: California
	 Candidate:Hillary Clinton Votes: 1940580
State: Colorado
	 Candidate:Bernie Sanders Votes: 72078
State: Connecticut
	 Candidate:Hillary Clinton Votes: 170075
State: Delaware
 	Candidate:Hillary Clinton Votes: 119368
State: Florida
 	Candidate:Hillary Clinton Votes: 1097449
State: Georgia
 	Candidate:Hillary Clinton Votes: 543076
State: Hawaii
	 Candidate:Bernie Sanders Votes: 23531
State: Idaho
 	Candidate:Ted Cruz Votes: 100942
State: Illinois
	 Candidate:Hillary Clinton Votes: 1012175
State: Indiana
	Candidate:Donald Trump Votes: 598045
State: Iowa
	Candidate:Bernie Sanders Votes: 72650
State: Kansas
	Candidate:Ted Cruz Votes: 35207


* RepresentCandidate:

We can see, how we know it, the candidate for republican is Donald Trump and for democrat is Hillary Clinton.

* Party name: Democrat
	Candidate:Hillary Clinton Votes: 15692452
* Party name: Republican
	Candidate:Donald Trump Votes: 13302541